import tensorflow as tf
from modelUtils import pixel_wise_softmax, dice_coe_c1, dice_hard_coe_c1, addSummaryScalar
import logging
from unetModels import ResUNet, UNet, ResUNetDS


class RootNet(object):
    def __init__(self, sess, config, name, isTrain):
        """
        :param sess:
        :param config:
        :param name:
        :param isTrain:
        """
        
        self.sess = sess
        self.name = name
        self.isTrain = isTrain
        self.summary = None
        self.summaryComponents = []

        if "finetuneLayers" in config:
            self.finetuneLayers = config["finetuneLayers"]
        else:
            self.finetuneLayers = None

        logging.info("Constructing RootNet Model")

        imShape = [config['batchSize']] + config['tileSize'] + [1]
        logging.info("Batch Shape:")
        logging.info(imShape)
        gtShape = [config['batchSize']] + config['tileSize'] + [2]

        # If phase = True, it is training phase. Otherwise is testing. It is used for batch_norm.
        self.phase = tf.placeholder(tf.bool, name='phase')
        self.learning_rate = config['learning_rate']
        logging.info("Learning Rate")
        logging.info(self.learning_rate)
        self.dropout = config['dropout']
        logging.info("Dropout")
        logging.info(self.dropout)

        logging.info("Model:")

        # Shared part of the network across multiple levels
        if config['Model'] == "ResUNet":
            print("Model: ResUNet")
            self.unet = ResUNet("ResUNet", self.finetuneLayers, self.dropout)
            logging.info("ResUNet")
        elif config['Model'] == "UNet":
            print("Model: UNet")
            self.unet = UNet("UNet", self.finetuneLayers, self.dropout)
            logging.info("UNet")
        elif config['Model'] == "ResUNetDS":
            print("Model: ResUNetDS")
            self.unet = ResUNetDS("UNet", self.finetuneLayers, self.dropout)
            logging.info("ResUNetDS")

        # Input image
        self.x = tf.placeholder(tf.float32, imShape, name='x')

        # GT Image
        self.y = tf.placeholder(tf.float32, gtShape, name='y')

        if config['Model'] == "ResUNetDS":
            self.output, self.middle = self.unet(self.x, isTrain=self.phase)
            self.m_logits = pixel_wise_softmax(self.middle)
            #self.m_logits = self.middle
        else:
            self.output = self.unet(self.x, isTrain=self.phase)
        
        self.logits = pixel_wise_softmax(self.output)

        if self.isTrain:
            regularizer = tf.add_n([tf.nn.l2_loss(v) for v in self.unet.varList if 'bias' not in v.name])

            if config['loss'] == "cross_entropy":
                self.loss = -tf.reduce_mean(self.y*tf.log(tf.clip_by_value(self.logits,1e-10,1.0)), name="cross_entropy")
            else:
                self.soft_dice = (1 - dice_coe_c1(self.logits, self.y))
                self.loss = self.soft_dice
            
            if config['Model'] == "ResUNetDS":
                self.m_loss = -tf.reduce_mean(self.y*tf.log(tf.clip_by_value(self.m_logits,1e-10,1.0)), name="cross_entropy")
                self.loss = self.loss + self.m_loss
                addSummaryScalar('Training_Middle_Loss', self.m_loss, self.summaryComponents)
            
            self.loss = self.loss + config["l2"] * regularizer
            
            self.hard_dice = dice_hard_coe_c1(self.logits, self.y)
            self.auc = tf.metrics.auc(self.y, self.logits, summation_method='careful_interpolation')

            #addSummaryScalar('Training_Loss', self.loss, self.summaryComponents)
            #addSummaryScalar('Training_AUC_ROC', self.auc[1], self.summaryComponents)
            #addSummaryScalar('Training_Dice', self.hard_dice, self.summaryComponents)
            
            update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)

            with tf.control_dependencies(update_ops):
                optim = tf.train.AdamOptimizer(name="optim", learning_rate=self.learning_rate)
                self.train = optim.minimize(self.loss, var_list=self.unet.varList)

        self.sess.run(tf.group(tf.global_variables_initializer(), tf.local_variables_initializer()))

        if len(self.summaryComponents) > 1:
            self.summary = tf.summary.merge(self.summaryComponents)

    def fit(self, batchX, batchY, summary=None, phase=1):
        if summary is None:
            _, loss= self.sess.run([self.train, self.loss], {self.x: batchX, self.y: batchY, self.phase: phase})
        else:
            log, _, loss= self.sess.run([summary, self.train, self.loss], {self.x: batchX, self.y: batchY, self.phase: phase})

        if summary is None:
            return loss
        else:
            return log, loss

    def deploy(self, batchX, batchY, phase=0):
        loss, dice, auc= self.sess.run([self.loss, self.hard_dice, self.auc], {self.x: batchX, self.y: batchY, self.phase: phase})
        return loss, dice, auc

    def segment(self, batchX):
        segmented= self.sess.run(self.logits, {self.x: batchX, self.phase: 0})
        return segmented

    def save(self, dir_path):
        self.unet.save(self.sess, dir_path + "/model.ckpt")

    def restore(self, dir_path):
        self.unet.restore(self.sess, dir_path + "/model.ckpt")
