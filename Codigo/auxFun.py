#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 14 12:23:38 2019

@author: ngaggion
"""
import cv2
import pathlib
import numpy as np
from PIL import Image


import re
def natural_key(string_):
    """See http://www.codinghorror.com/blog/archives/001018.html"""
    return [int(s) if s.isdigit() else s for s in re.split(r'(\d+)', string_)]


def load_path(search_path, ext = '*.*'):
    data_root = pathlib.Path(search_path)
    all_files = list(data_root.glob(ext))
    all_files = [str(path) for path in all_files]
    all_files.sort(key = natural_key)
    
    return all_files


def check_integrity(all_files):
    lista = []
    files = []
    
    for i in range(len(all_files)):
        print(i)
        try:
            im = np.array(Image.open(all_files[i]).convert('L'), np.float32)
            im.verify() #I perform also verify, don't know if he sees other types o defects
            im.close() #reload is necessary in my case
            files.append(all_files[i])
        except:
            lista.append(all_files[i])
    
    with open('broken_files.txt', '+w') as f:
        for item in lista:
            f.write("%s\n" % item)
        
    return files


def selectROI(image):
    y, x = image.shape[0:2]
    y = y//4
    x = x//4
    
    cv2.namedWindow("Image",cv2.WINDOW_NORMAL)
    cv2.resizeWindow("Image", x, y)
    r = cv2.selectROI("Image",image)
    
    cv2.waitKey()
    cv2.destroyAllWindows()
    return r


pos = []

def mouse_callback(event,x,y,flags,param):
    global pos
    if event == cv2.EVENT_LBUTTONDOWN:
        pos = [(x, y)]
        print(pos)

def selectSeed(image):
    global pos
    y, x = image.shape[0:2]
    y = y//2
    x = x//2
    
    clone = image.copy()
    
    cv2.namedWindow("Image",cv2.WINDOW_NORMAL)
    cv2.resizeWindow("Image", x, y)
    cv2.setMouseCallback('Image',mouse_callback) #Mouse callback
    
    while True:
        if pos != []:
            cv2.circle(clone,pos[0],6,[255,0,0],-1)
        
    	# display the image and wait for a keypress
        cv2.imshow("Image", clone)
        key = cv2.waitKey(1) & 0xFF
     
        # if the 'r' key is pressed, reset the cropping region
        if key == ord("r"):
            clone = image.copy()
     
        # if the 'c' key is pressed, break from the loop
        elif key == ord("c"):
            break
        
        elif key == 13:
            break
    
    cv2.destroyAllWindows()
    return pos


def prune(skel, num_it):
    orig = skel
    
    endpoint1 = np.array([[-1, -1, -1],
                          [-1, 1, -1],
                          [0, 1, 0]])
    
    endpoint2 = np.array([[0, 1, 0],
                          [-1, 1, -1],
                          [-1, -1, -1]])
    
    endpoint4 = np.array([[0, -1, -1],
                          [1, 1, -1],
                          [0, -1, -1]])
    
    endpoint5 = np.array([[-1, -1, 0],
                          [-1, 1, 1],
                          [-1, -1, 0]])
    
    endpoint3 = np.array([[-1, -1, 1],
                          [-1, 1, -1],
                          [-1, -1, -1]])
    
    endpoint6 = np.array([[-1, -1, -1],
                          [-1, 1, -1],
                          [1, -1, -1]])
    
    endpoint7 = np.array([[-1, -1, -1],
                          [-1, 1, -1],
                          [-1, 1, -1]])
    
    endpoint8 = np.array([[1, -1, -1],
                          [-1, 1, -1],
                          [-1, -1, -1]])
    
    
    for i in range(0, num_it):
        ep1 = skel - cv2.morphologyEx(skel, cv2.MORPH_HITMISS, endpoint1)
        ep2 = ep1 - cv2.morphologyEx(ep1, cv2.MORPH_HITMISS, endpoint2)
        ep3 = ep2 - cv2.morphologyEx(ep2, cv2.MORPH_HITMISS, endpoint3)
        ep4 = ep3 - cv2.morphologyEx(ep3, cv2.MORPH_HITMISS, endpoint4)
        ep5 = ep4 - cv2.morphologyEx(ep4, cv2.MORPH_HITMISS, endpoint5)
        ep6 = ep5 - cv2.morphologyEx(ep5, cv2.MORPH_HITMISS, endpoint6)
        ep7 = ep6 - cv2.morphologyEx(ep6, cv2.MORPH_HITMISS, endpoint7)
        ep8 = ep7 - cv2.morphologyEx(ep7, cv2.MORPH_HITMISS, endpoint8)
        skel = ep8
        
    end =  endPoints(skel)
    kernel_size = 3
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(kernel_size,kernel_size))
    
    for i in range(0, num_it):
        end = cv2.dilate(end, kernel)
        end = cv2.bitwise_and(end, orig)
        
    return cv2.bitwise_or(end, skel)


def cleanUp(it, thresh, seed): 
    thresh[0:seed[0][1],:] = 0
    
    kernel_size = 3
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(kernel_size,kernel_size))
    thresh = cv2.dilate(thresh,kernel)
    thresh = cv2.erode(thresh, kernel)
    thresh = cv2.erode(thresh, kernel)
    thresh = cv2.dilate(thresh, kernel)
    
    it = 0
    if it > 100:
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        for i in contours:
            contour_sizes = [(cv2.contourArea(contour), contour) for contour in contours]
            biggest_contour = max(contour_sizes, key=lambda x: x[0])[1]
            #define a mask
            mask = np.zeros(thresh.shape, np.uint8)
            cv2.drawContours(mask,[biggest_contour], -1, 255, -1) 
            thresh = cv2.bitwise_and(mask,thresh)

    return thresh

def getROIandSeed(raw_files, seg_files, showSeg = False):
    N1 = len(raw_files)
    N2 = len(seg_files)

    N = min(N1,N2)
    print(N)
    original = cv2.imread(raw_files[N-1])
    
    if showSeg:
        seg = cv2.imread(seg_files[N-1],0) 
        original[:,:,0] = cv2.add(original[:,:,0], seg)
    
    r = selectROI(original)
    p = np.array([int(r[1]),int(r[1]+r[3]), int(r[0]),int(r[0]+r[2])])
    
    i = 0
    img = cv2.imread(raw_files[i])
    boundingBox = img[p[0]:p[1],p[2]:p[3]]
    
    seed = selectSeed(boundingBox)
    
    return p, seed


def branchedPoints(skel):
    X=[]
    #cross X
    X0 = np.array([[0, 1, 0], [1, 1, 1], [0, 1, 0]])
    X1 = np.array([[1, 0, 1], [0, 1, 0], [1, 0, 1]])
    X.append(X0)
    X.append(X1)
    
    #T like
    T=[]
    T0=np.array([[2, 1, 2], 
                 [1, 1, 1], 
                 [2, 2, 2]]) # T0 contains X0
    T1=np.array([[1, 2, 1], [2, 1, 2], [1, 2, 2]]) # T1 contains X1
    T2=np.array([[2, 1, 2], [1, 1, 2], [2, 1, 2]])
    T3=np.array([[1, 2, 2], [2, 1, 2], [1, 2, 1]])
    T4=np.array([[2, 2, 2], [1, 1, 1], [2, 1, 2]])
    T5=np.array([[2, 2, 1], [2, 1, 2], [1, 2, 1]])
    T6=np.array([[2, 1, 2], [2, 1, 1], [2, 1, 2]])
    T7=np.array([[1, 2, 1], [2, 1, 2], [2, 2, 1]])
    
    T.append(T0)
    T.append(T1)
    T.append(T2)
    T.append(T3)
    T.append(T4)
    T.append(T5)
    T.append(T6)
    T.append(T7)
    
    #Y like
    Y=[]
    Y0=np.array([[1, 0, 1], [0, 1, 0], [2, 1, 2]])
    Y1=np.array([[0, 1, 0], [1, 1, 2], [0, 2, 1]])
    Y2=np.array([[1, 0, 2], [0, 1, 1], [1, 0, 2]])
    Y3=np.array([[0, 2, 1], [1, 1, 2], [0, 1, 0]])
    Y4=np.array([[2, 1, 2], [0, 1, 0], [1, 0, 1]])
    Y5 = np.rot90(Y3)
    Y6 = np.rot90(Y4)
    Y7 = np.rot90(Y5)
    
    Y.append(Y0)
    Y.append(Y1)
    Y.append(Y2)
    Y.append(Y3)
    Y.append(Y4)
    Y.append(Y5)
    Y.append(Y6)
    Y.append(Y7)
    
    bp = np.zeros(skel.shape, dtype=int)
    for x in X:
        bp = bp + cv2.morphologyEx(skel, cv2.MORPH_HITMISS, x)
    for y in Y:
        bp = bp + cv2.morphologyEx(skel, cv2.MORPH_HITMISS, y)
    for t in T:
        bp = bp + cv2.morphologyEx(skel, cv2.MORPH_HITMISS, t)
        
    return bp


def endPoints(skel):
    endpoint1=np.array([[1, -1, -1],
                        [-1, 1, -1],
                        [-1, -1, -1]])
    
    endpoint2=np.array([[-1, 1, -1],
                        [-1, 1, -1],
                        [-1, -1, -1]])
    
    endpoint3=np.array([[-1, -1, 1],
                        [-1, 1, -1],
                        [-1, -1, -1]])
    
    endpoint4=np.array([[-1, -1, -1],
                        [1, 1, -1],
                        [-1, -1, -1]])
    
    endpoint5=np.array([[-1, -1, -1],
                        [-1, 1, 1],
                        [-1, -1, -1]])
    
    endpoint6=np.array([[-1, -1, -1],
                        [-1, 1, -1],
                        [1, -1, -1]])
    
    endpoint7=np.array([[-1, -1, -1],
                        [-1, 1, -1],
                        [-1, 1, -1]])
    
    endpoint8=np.array([[-1, -1, -1],
                        [-1, 1, -1],
                        [-1, -1, 1]])
    
    ep1 = cv2.morphologyEx(skel, cv2.MORPH_HITMISS, endpoint1)
    ep2 = cv2.morphologyEx(skel, cv2.MORPH_HITMISS, endpoint2)
    ep3 = cv2.morphologyEx(skel, cv2.MORPH_HITMISS, endpoint3)
    ep4 = cv2.morphologyEx(skel, cv2.MORPH_HITMISS, endpoint4)
    ep5 = cv2.morphologyEx(skel, cv2.MORPH_HITMISS, endpoint5)
    ep6 = cv2.morphologyEx(skel, cv2.MORPH_HITMISS, endpoint6)
    ep7 = cv2.morphologyEx(skel, cv2.MORPH_HITMISS, endpoint7)
    ep8 = cv2.morphologyEx(skel, cv2.MORPH_HITMISS, endpoint8)
    
    ep = ep1+ep2+ep3+ep4+ep5+ep6+ep7+ep8
    return ep


def trim(ske):
    #T like
    T=[]
    T0=np.array([[-1, 1, -1], 
                 [1, 1, 1], 
                 [-1, -1, -1]]) # T0 contains X0
    T2=np.array([[-1, 1, -1], 
                 [1, 1, -1], 
                 [-1, 1, -1]])
    T4=np.array([[-1, -1, -1], 
                 [1, 1, 1], 
                 [-1, 1, -1]])
    T6=np.array([[-1, 1, -1], 
                 [-1, 1, 1], 
                 [-1, 1, -1]])
    
    T.append(T0)
    T.append(T2)
    T.append(T4)
    T.append(T6)
    
    bp = np.zeros_like(ske)
    for t in T:
        bp = bp + cv2.morphologyEx(ske, cv2.MORPH_HITMISS, t)
    
    ske = cv2.subtract(ske, bp)    
    
    return ske


def skeleton_nodes(ske):
    branch = branchedPoints(ske)
    end = endPoints(ske)
    
    bp = np.where(branch == 1)
    bnodes = []
    for i in range(len(bp[0])):
        bnodes.append([bp[1][i],bp[0][i]])
    
    ep = np.where(end == 1)
    enodes = []
    for i in range(len(ep[0])):
        enodes.append([ep[1][i],ep[0][i]])
    
    return np.array(bnodes), np.array(enodes)


def correct_segmentation(conf, masks, folder):
    shape = cv2.imread(masks[0],0).shape
    accum = np.zeros(shape, np.float32)
    
    t = len(masks)
    
    thresh = conf['Thresh']
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(2,2))
    
    for i in range(t):
        img1 = cv2.imread(masks[i],0) / 255.0
                
        accum = img1 + conf['Alpha'] * accum
        
        morphed = cv2.erode(accum, kernel)
        
        _, seg = cv2.threshold(morphed, thresh, 1.0, cv2.THRESH_BINARY)
        
        name = masks[i].replace(conf['SegPath'], folder)
        cv2.imwrite(name, np.uint8(seg*255))
        print(i)
        
    return 
