#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 14 16:20:40 2019

@author: ngaggion
"""

import numpy as np
import graph_tool.all as gt
import cv2
from skimage.morphology import skeletonize
from auxFun import skeleton_nodes, cleanUp, trim, prune


def unit_vector(vector):
    return vector / np.linalg.norm(vector)


def angle_between(v1, v2):
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))


def vecinos(img,seed):
    lista = []
    x = seed[0]
    y = seed[1]
    
    ymax = img.shape[0]
    xmax = img.shape[1]
    
    for i in range (y+1,y-2,-1):
        for j in range(x-1,x+2):
            if i < ymax and j < xmax:
                if img[i,j] == 1:
                    if x!=j or y!=i:
                        lista.append([j,i])
    return lista


def find_nearest(node, lnodes): #RETURNS THE LIST OF NODES WITHOUT THE NEAREST ONE
    d = np.linalg.norm(node-lnodes, axis = 1)
    p = np.argmin(d)
    nearest = lnodes[p,:]
    lnodes = np.delete(lnodes, p, axis = 0)

    return nearest, lnodes, d

c = 0

def get_next_node(img, actual, padre, hermanos, d):
    global c
    hijos = vecinos(img, actual)
    sons = []
    
    for j in hijos:
        if not np.array_equal(j, padre) and not(j in hermanos):
            sons.append(j)
        
    if len(sons) != 1:
        return [img, actual, d]
    
    img[actual[1], actual[0]] = c
    hijo = sons[0]
    dist = np.linalg.norm(np.array(actual) - np.array(hijo)) + d
    
    return get_next_node(img, hijo, actual, hijos, dist)



def continue_graph(g, pos, weight, clase, img, actual, padre, enodes, bnodes):
    # padre refiere al indice en el grafo de graph_tool
    # actual es la posiciÃ³n del nodo en la imagen
    global c
    
    hijos = vecinos(img, actual)
    
    for i in hijos:
        img, nodo, largo_arista = get_next_node(img, i, actual, hijos, 0)
        if largo_arista != 0:
            s = gt.find_vertex(g, pos,nodo)
            if s == []:
                s = g.add_vertex()
                pos[s] = nodo
            else:
                s = s[0]
                
            arista = g.add_edge(padre, s)
            weight[arista] = largo_arista
            clase[arista] = [c, 0]
            
            c = c+1
            
            if nodo not in enodes.tolist():
                g, pos, weight = continue_graph(g, pos, weight, clase, img, nodo, s, enodes, bnodes)

    return g, pos, weight
    

def create_graph(img, seed, enodes, bnodes, init = True):
    g = gt.Graph(directed = False)
    enodes = np.array(enodes)
    bnodes = np.array(bnodes)
    pos = g.new_vertex_property("vector<int>")
    nodetype = g.new_vertex_property("string")
    age = g.new_vertex_property("int")
    
    weight = g.new_edge_property("float")
    clase = g.new_edge_property("vector<int>") #[color, tipo]

    global c
    global dic
    
    c = 2
    dic = {}
    
    s1, enodes, d = find_nearest(seed, enodes)
    
    if np.min(d) > 25 and init == False:
        raise Exception ("No seed")
    
    semilla = g.add_vertex()
    pos[semilla] = s1
    
    hijos = vecinos(img, s1)

    img, nodo, largo_arista = get_next_node(img, hijos[0], s1, [], 0)
    
    if largo_arista != 0:
        n2 = g.add_vertex()    
        pos[n2] = nodo
        arista = g.add_edge(semilla, n2)
        weight[arista] = largo_arista
        clase[arista] = [c, 0]
        
        c = c+1

        if nodo not in enodes.tolist():
            g, pos, weight = continue_graph(g, pos, weight, clase, img, nodo, n2, enodes, bnodes)

    v = g.get_vertices()
    if len(v) < 2:
        raise Exception("No seed")
    for i in v:
        age[i] = 0
        nodetype[i] = "null"
    
    grafo = [g, pos, weight, clase, nodetype, age]
    
    return grafo, s1, img


def graph_init(graph):
    g, pos, weight, clase, nodetype, age = graph
    
    vertices = g.get_vertices()
    
    pos_vertex = []
    for i in vertices:
        pos_vertex.append(pos[i])
    pos_vertex = np.array(pos_vertex)
    
    seed = pos_vertex[np.argmin(pos_vertex[:,1]),:]
    tip = pos_vertex[np.argmax(pos_vertex[:,1]),:]
    
    for i in vertices:
        p1 = pos_vertex[i]
        if np.array_equal(p1, seed):
            nodetype[i] = "Ini"
        else:
            if np.array_equal(p1, tip):
                nodetype[i] = "FTip"
            else:
                vecinos = g.get_out_neighbours(i)
                if len(vecinos) > 1:
                    nodetype[i] = "Bif"
                else:
                    if len(vecinos) == 1:
                        nodetype[i] = "LTip"
                
    
    clase[g.edge(0,1)][1] = 10
    age[g.vertex(0)] = 1
    age[g.vertex(1)] = 1

    return [g, pos, weight, clase, nodetype, age]


def get_Graph(it, segmentation, p, seed, orig_seed, init = True):
    seg = cv2.imread(segmentation, 0)[p[0]:p[1],p[2]:p[3]]
    
    clean = cleanUp(it, seg, orig_seed)
    
    ske = np.array(skeletonize(clean // 255), dtype = 'uint8')
    ske = trim(ske)
    
    if it > 50:
        ske = prune(ske,8)
    
    bnodes, enodes = skeleton_nodes(ske) #ALWAYS X,Y

    ske2 = ske.copy()
    
    try:
        grafo, seed, ske2 = create_graph(ske2, seed, enodes, bnodes, init)
    except:
        return False, seed, ske, ske2, clean
    
    return grafo, seed, ske, ske2, clean


def plot_graph(grafo1, original, ske2):
    if grafo1 is not False:
        g, pos, _, _, nodetype, _ = grafo1
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (4,4))
        grafo = cv2.dilate(ske2, kernel)
    
        grafo_img = 255 - 100 * grafo #/ m
        grafo_color = cv2.cvtColor(grafo_img, cv2.COLOR_GRAY2BGR)

        for j in g.get_vertices():
            if nodetype[j] == "FTip" or nodetype[j] == "Ini":
                cv2.circle(grafo_color, tuple(pos[j]), 5, (0,255,0), -1)
            elif nodetype[j] == "LTip":
                cv2.circle(grafo_color, tuple(pos[j]), 5, (0,0,255), -1)
            else:
                cv2.circle(grafo_color, tuple(pos[j]), 5, (0,200,200), -1)
    
        #image = np.concatenate([original, grafo_color], axis = 1)
    
        image = grafo_color
    else:
        image = ske2.astype('uint8') * 0 + 255
    return image


def find_nearest_b(node, lnodes): #RETURNS THE LIST OF NODES WITHOUT THE NEAREST ONE
    d = np.linalg.norm(node-lnodes, axis = 1)
    p = np.argmin(d)

    return p, d[p]


def match_graph(graph1, graph2):
    g1, pos1, weight1, clase1, nodetype1, age1 = graph1
    g2, pos2, weight2, clase2, nodetype2, age2 = graph2
    
    vertices1 = g1.get_vertices()
    vertices2 = g2.get_vertices()
    
    pos_vertex2 = []
    for i in vertices2:
        pos_vertex2.append(pos2[i])
    pos_vertex2 = np.array(pos_vertex2)
    
    for i in vertices1:
        p1 = pos1[i]
        v, d = find_nearest_b(p1, pos_vertex2)
        if d < 30:
            age2[v] = age1[i] + 1            
            if nodetype2[v] == "null":
                nodetype2[v] = nodetype1[i]

    for i in vertices2:
        if age2[i] == 0:
            age2[i] == 1
        if nodetype2[i] == "null":
            vecinos = g2.get_out_neighbours(i)
            if len(vecinos) > 1:
                nodetype2[i] = "Bif"
            else:
                if len(vecinos) == 1:
                    nodetype2[i] = "LTip"
                else:
                    raise Exception ("BAD TRACKING")
    
    
    seed = gt.find_vertex(g2, nodetype2, "Ini")
    if len(seed) == 1:
        seed = seed[0]
    else:
        seed_prev = gt.find_vertex(g1, nodetype1, "Ini")
        p1 = pos1[seed_prev[0]]
        v, d = find_nearest_b(p1, pos_vertex2)
        if d < 50:
            pos_vertex2 = np.delete(pos_vertex2, v, axis = 0)
            age2[v] = age1[seed_prev[0]] + 1
            nodetype2[v] = "Ini"
            seed = v
        else:
            print('No SEED')
            raise Exception ("BAD TRACKING")
    
    end = gt.find_vertex(g2, nodetype2, "FTip")
    if len(end) == 1:
        end = end[0]
    else:
        end_prev = gt.find_vertex(g1, nodetype1, "FTip")
        p1 = pos1[end_prev[0]]
        v, d = find_nearest_b(p1, pos_vertex2)
        if d < 100:
            pos_vertex2 = np.delete(pos_vertex2, v, axis = 0)
            age2[v] = age1[end_prev[0]] + 1
            nodetype2[v] = "FTip"
            end = v
        else:
            print('No TIP')
            raise Exception ("BAD TRACKING")
    
    paths = gt.all_shortest_paths(g2, seed, end, weights = weight2)
    c = 0
    caminos = []
    for i in paths:
        c +=1
        caminos.append(i)
    
    if c == 1:
        camino = caminos[0]
        l = len(camino)
        for k in range(0, l-1):
            arista = g2.edge(camino[k], camino[k+1])
            clase2[arista][1] = 10
            
    else:
        camino = caminos[0]
        l = len(camino)
        for k in range(0, l-1):
            arista = g2.edge(camino[k], camino[k+1])
            clase2[arista][1] = 10

    return [g2, pos2, weight2, clase2, nodetype2, age2]


def save_props(it, grafo, csv_writer):
    if grafo is not False:
        g, pos, weight, clase, nodetype, age = grafo
        
        main_root_len = 0
        sec_root_len = 0
        number_lateral_roots = 0
        tot_len = 0
        
        for i in g.get_vertices():
            if nodetype[i] == "LTip" and age[i] > 10:
                number_lateral_roots += 1
            
        for i in g.edges():
            tot_len += weight[i]
            if clase[i][1] == 10:
                main_root_len += weight[i]
            elif age[i.source()] > 10 or age[i.target()] > 10:
                sec_root_len += weight[i]
        
        row = [it, main_root_len, sec_root_len, number_lateral_roots, tot_len]
    else:
        row = [it, 0, 0, 0, 0]
    csv_writer.writerow(row)
    return
