#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep  4 11:49:50 2019

@author: ngaggion
"""

from auxFun import load_path, check_integrity, correct_segmentation, getROIandSeed
import sys
import os

from rootTrack import rootTrack
from Model import RootNet
import tensorflow as tf
from PIL import Image
import skimage.io
import numpy as np


def save_image_with_scale(path, arr):
  arr = np.clip(arr, 0., 1.)
  arr = arr * 255.
  arr = arr.astype(np.uint8)
  skimage.io.imsave(path, arr)


def SaveSegImage(name, segmentation, path, suffix = '.png'):
    segmentacion = np.argmax(segmentation[0,:,:,:], axis = 2)
    segmentacion = segmentation[0,:,:,1]
    name = name.replace(suffix,"_mask.png")
    nombre = os.path.join(path, name)
    save_image_with_scale(nombre, segmentacion)
    
    
def SegmentUNet(all_images, model, suffix = '.png'):
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

    sess = tf.Session()

    conf = {}
    file = exec(open('confs/train_conf.conf').read(),conf)
    
    conf["Model"] = "ResUNetDS"
    conf["batchSize"] = 1
    
    data = np.array(Image.open(all_images[0]).convert('L'), dtype=np.float32)/255.0
    tensor = np.zeros([1, data.shape[0], data.shape[1],1])
    conf["tileSize"] = list(data.shape[0:2])

    net = RootNet(sess, conf, "RootNET", True)
    
    print("loaded")
    
    net.restore(model)

    print('RESTORED')

    for i in range(0, len(all_images)):
        print(i)
        tensor[0,:,:,0] = np.array(Image.open(all_images[i]).convert('L'), dtype=np.float32)/255.0
        segment = net.segment(tensor)
        SaveSegImage(all_images[i], segment, suffix)
        
    tf.reset_default_graph()
    sess.close()    
    print("Session ended succesfully")
    
    return segment
    


if __name__ == "__main__":
    conf = {}
    
    try:
        os.mkdir(conf['Project'])
        print('Project folder created')
    except:
        pass
    
    if len(sys.argv) > 1:
        file = sys.argv[1]
        file = exec(open(file).read(),conf)
    else:
        file = exec(open('confs/config_file.conf').read(),conf)

    all_files = load_path(conf['Path'], "*.png") 
    all_files = [file for file in all_files if 'mask' not in file]

    if conf['CheckIntegrity']:
        all_files = check_integrity(all_files)
    
    if conf['SegPath'] is None:
        a = SegmentUNet(all_files, model = conf['Model'])
        conf['SegPath'] = conf['Path']
    
    
    all_files = load_path(conf['SegPath'], "*mask.png") 
    masks = [file for file in all_files if 'mask' in file]
    
    if conf['AutoCorrect']:
        print('Starting autocorrection')
        folder = os.path.join(conf['Project'],'CorrectedSeg')
        print(folder)
        try:
            os.mkdir(folder)
            correct_segmentation(conf, masks, folder)
        except:
            pass

        all_files = load_path(folder, "*mask.png")
        masks = [file for file in all_files if 'mask' in file]
    
    ext = "*" + conf["FileExt"]
    images = load_path(conf['Path'], ext)
    images = [file for file in images if 'mask' not in file]
    
    for i in range(0, conf['NumberOfPlants']):
        p, seed = getROIandSeed(images,masks) 
        rootTrack(i, conf, images, masks, p, seed)
