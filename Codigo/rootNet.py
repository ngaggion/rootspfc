import tensorflow as tf
import logging
import os
import gc
import numpy as np
import medpy.metric.binary as metric
import skimage.io

from Model import RootNet
from BatchGenerator import Patch2DBatchGeneratorFromTensors
from Provider import MPImageDataProvider, DataProvider, ImageDataProvider
from Model_Util import addScalarValueToSummary


def mkdir(dir_path):
  try :
    os.makedirs(dir_path)
  except: pass 


def save_image_as_it_is(path, arr):
  skimage.io.imsave(path, arr)
 
    
def save_image_with_scale(path, arr):
  arr = np.clip(arr, 0., 1.)
  arr = arr * 255.
  arr = arr.astype(np.uint8)
  skimage.io.imsave(path, arr)

def padImgToMakeItMultipleOf(v, multipleOf=[8, 8], mode='symmetric'):
    padding = ((0, 0 if v.shape[0] % multipleOf[0] == 0 else multipleOf[0] - (v.shape[0] % multipleOf[0])),
               (0, 0 if v.shape[1] % multipleOf[1] == 0 else multipleOf[1] - (v.shape[1] % multipleOf[1])))
    return np.pad(v, padding, mode)


def setupLogger(logFilename):
    # set up logging to file - see previous section for more details
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)-10s %(levelname)-8s %(message)s',
                        datefmt='%m-%d %H:%M',
                        filename=logFilename,
                        filemode='w+')
    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

    # Now, we can log to the root logger, or any other logger. First the root...
    print ('Logging to file: %s' % logFilename)
    logging.info('Logging to file: %s' % logFilename)



"""
    Training RootNet
"""


def TrainUNet(conf, train_paths, val_paths):
    gc.collect()

    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
    mkdir(conf["outDir"])
    mkdir(conf["tmpDir"])
    mkdir(conf["modelDir"])
    mkdir(conf["ckptDir"])
    mkdir(conf["finalDir"])

    sess = tf.Session()

    name = conf["session"]
    # Create the network
    net = RootNet(sess, conf, "RootNET", True)
    #net.restore(conf["finalDir"])
    train_writer = tf.summary.FileWriter(os.path.join(conf["outDir"], name), sess.graph)

    random_state = np.random.RandomState(63)

    # Load the data and create the Batch Generator

    TrainProviderAug = MPImageDataProvider(train_paths, augment = True, shuffle_data = True)
    trainFilesAug = len(TrainProviderAug.data_files) * 4
    logging.info('Raw Training Files: %s' %len(TrainProviderAug.data_files))
    logging.info('Total Training Files: %s' %trainFilesAug)
    data_aug, gt_aug = TrainProviderAug(trainFilesAug)  
    
    batchGenTrainAug = Patch2DBatchGeneratorFromTensors(conf, data_aug, gt_aug, random_state = random_state, maxQueueSize = 25, infiniteLoop = True)
    batchGenTrainAug.generateBatches()
    
    ValProvider = MPImageDataProvider(val_paths)
    valFiles = len(ValProvider.data_files)
    logging.info('Validation Files: %s' %valFiles)
    data_val, gt_val = ValProvider(valFiles)

    batchGenVal = Patch2DBatchGeneratorFromTensors(conf, data_val, gt_val, random_state = random_state, maxQueueSize = 25, infiniteLoop = True)
    batchGenVal.generateBatches()

    maxDice = 0
    ep = 0

    # Train the network
    for i in range(1,conf['numEpochs']+1):
        print('Epoch', i)
        
        meanLoss = 0
        
        for j in range(0, trainFilesAug//4): #SubEpoch
            print('SubEpoch', j)
            dTrain, gTrain = batchGenTrainAug.getBatch()
            lossTrain = net.fit(dTrain, gTrain)
            meanLoss = meanLoss + lossTrain

        meanLoss = 4 * meanLoss / trainFilesAug
        addScalarValueToSummary("Training_Loss", meanLoss, train_writer, i)

        print('Validation Process')
        dic = 0
        l = 0
        aucv = 0

        for j in range(0, valFiles): #SubEpoch
            print('Validation SubEpoch', j)
            dVal, gVal = batchGenVal.getBatch()
            lossVal, dice, auc = net.deploy(dVal, gVal)
            
            dic = dic + dice
            l = l + lossVal
            aucv = aucv + auc[1]

        dic = dic/(valFiles)
        l = l/(valFiles)
        aucv = aucv/(valFiles)

        addScalarValueToSummary("Validation_Dice", dic, train_writer, i)
        addScalarValueToSummary("Validation_AUC_ROC", aucv, train_writer, i)
        addScalarValueToSummary("Validation_Loss", l, train_writer, i)           
        
        if dic > maxDice:
            print('MODEL SAVED')
            ep = i
            net.save(conf["ckptDir"])
            maxDice = dic

    print('Best Model at epoch: ',ep)
    net.save(conf["finalDir"])

    #batchGenTrain.finish()
    batchGenTrainAug.finish()
    batchGenVal.finish()
    tf.reset_default_graph()
    sess.close()
    print("Session ended succesfully")


def TestUNet(conf, path, model = "finalDir"):
    gc.collect()
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

    conf["batchSize"] = 1

    dice = []
    hd = []
    spath = os.path.join(conf['modelDir'], "Test")
    spath = os.path.join(spath, model)
    mkdir(spath)

    for j in range(0,len(path)):
        Provider = ImageDataProvider(path[j])
        sess = tf.Session()
        data, mask = Provider(1)
                
        data = data[0]
        mask = mask[0]

        data = padImgToMakeItMultipleOf(data)        
        ny = data.shape[0]
        nx = data.shape[1]
        
        X = np.zeros((1, ny, nx, 1))
    
        X[0,:,:,0] = data/255.0

        mask = mask[:,:,1]
        mask = padImgToMakeItMultipleOf(mask)
        
        conf["tileSize"] = list(data.shape[0:2])
        net = RootNet(sess, conf, "RootNET", True)
        net.restore(conf[model])
        
        segment = net.segment(X)
        segmentacion = np.argmax(segment[0,:,:,:], axis = 2)

        save_image_as_it_is(os.path.join(spath,"root_%s_%s.png" %(j,0)), data.astype('uint8'))
        save_image_with_scale(os.path.join(spath,"root_%s_%s_m.png" %(j,0)), mask)
        save_image_with_scale(os.path.join(spath,"root_%s_%s_s.png" %(j,0)), segmentacion)
        
        try:
            d = metric.dc(segmentacion,mask)
            dice.append(d)
            h = metric.hd(segmentacion,mask)
            hd.append(h)
        except:
            pass
        
        for i in range(1,len(Provider.data_files)):
            data, mask = Provider(1)
            
            data = data[0]
            mask = mask[0]
    
            data = padImgToMakeItMultipleOf(data)        
            ny = data.shape[0]
            nx = data.shape[1]
            
            X = np.zeros((1, ny, nx, 1))
        
            X[0,:,:,0] = data/255.0
    
            mask = mask[:,:,1]
            mask = padImgToMakeItMultipleOf(mask)
    
            segment = net.segment(X)
            segmentacion = np.argmax(segment[0,:,:,:], axis = 2)
    
            save_image_as_it_is(os.path.join(spath,"root_%s_%s.png" %(j,i)), data.astype('uint8'))
            save_image_with_scale(os.path.join(spath,"root_%s_%s_m.png" %(j,i)), mask)
            save_image_with_scale(os.path.join(spath,"root_%s_%s_s.png" %(j,i)), segmentacion)
    
            try:
                d = metric.dc(segmentacion,mask)
                dice.append(d)
                h = metric.hd(segmentacion,mask)
                hd.append(h)
            except:
                pass
        
        tf.reset_default_graph()
        sess.close()
    
    with open(os.path.join(conf['modelDir'],"dice_"+model+".txt"), "w+") as f:
        for item in dice:
            f.write("%s\n" % item)
    with open(os.path.join(conf['modelDir'],"hd_"+model+".txt"), "w+") as f:
        for item in hd:
            f.write("%s\n" % item)

    print("Session ended succesfully")
    return dice


def SaveSegImage(name, segmentation, path, suffix = None):
    segmentacion = np.argmax(segmentation[0,:,:,:], axis = 2)
    segmentacion = segmentation[0,:,:,1]
    name = name[0][0].replace(suffix,"_mask.png")
    nombre = os.path.join(path, name)
    save_image_with_scale(nombre, segmentacion)


def SegmentUNet(conf, path, suffix = ".png"):
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
      
    Provider = DataProvider(path, data_suffix = ".png") #, mask_suffix = "_mask.jpg")
    data, name = Provider(1)

    p = os.path.join(path,'seg')
    
    try:
        mkdir(p)
    except:
        pass
   
    sess = tf.Session()

    #conf["Model"] = "ResUNet"
    conf["batchSize"] = 1
    conf["tileSize"] = list(data.shape[1:3])

    net = RootNet(sess, conf, "RootNET", True)
    
    print("loaded")
    
    net.restore(conf["ckptDir"])

    print('RESTORED')

    for i in range(1,len(Provider.data_files)):
        print(i)
        data, name = Provider(1)
        segment = net.segment(data)
        SaveSegImage(name, segment, p, suffix)
        
    tf.reset_default_graph()
    sess.close()    
    print("Session ended succesfully")


if __name__ == "__main__":
    conf = {}
    file = exec(open('confs/train_conf.conf').read(),conf)

    print('Run')
    setupLogger(logFilename = conf['session'] + "log.log")

    logging.info("TrainPaths") 
    logging.info(conf['train_paths'])
    logging.info("ValPaths")
    logging.info(conf['val_paths'])
    logging.info("TestPaths")
    logging.info(conf['test_paths'])

    TrainUNet(conf, conf['train_paths'], conf['val_paths'])
    TestUNet(conf, conf['test_paths'], model = "ckptDir")
    TestUNet(conf, conf['test_paths'], model = "finalDir")
    
    # path = "/media/ncaggion/Datos/Raices/Video_1/1/"
    # conf['ckptDir'] = "/home/ncaggion/pc49/BestModel"
    # SegmentUNet(conf, path)


