#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 14 11:47:16 2019

@author: ngaggion
"""

import cv2
from graphFun import get_Graph, graph_init, plot_graph, save_props, match_graph
import csv
import os



def rootTrack(t, conf, raw_files, seg_files, p, seed):
    folder = os.path.join(conf['Project'], "Results %s" %t)
    
    try:
        os.mkdir(folder)
    except:
        pass
        
    if conf['SaveGraph']:
        graphImage = os.path.join(folder, "Grafos")
        failImage = os.path.join(folder, "Failed")
        try:
            os.mkdir(graphImage)
            os.mkdir(failImage)
        except:
            pass
    
    N1 = len(raw_files)
    N2 = len(seg_files)
    
    N = min(N1,N2)
    
    orig_seed = seed
    
    pfile = os.path.join(folder, "Results.csv")
    with open(pfile, 'w+') as csv_file:
        csv_writer = csv.writer(csv_file)	
        i = 0
        print(i)
        original = cv2.imread(raw_files[i])[p[0]:p[1],p[2]:p[3]]
        
        for j in range(1,N):
            print(j)
            grafo1, seed, ske, ske2, seg = get_Graph(i, seg_files[j], p, seed, orig_seed, False)
            if grafo1 is not False:
                i = j
                break
            if conf['SaveGraph']:
                image = plot_graph(grafo1, original, ske)
                #path = os.path.join(graphImage, 'out_1_%s.png' %j)
                #cv2.imwrite(path, original)
                #path = os.path.join(graphImage, 'out_2_%s.png' %j)
                #cv2.imwrite(path, seg)
                #path = os.path.join(graphImage, 'out_3_%s.png' %j)
                #cv2.imwrite(path, image)
            save_props(j, grafo1, csv_writer)
            
        print('Grow start')
        
        grafo1 = graph_init(grafo1)
        if conf['SaveGraph']:
            image = plot_graph(grafo1, original, ske2)
            #path = os.path.join(graphImage, 'out_1_%s.png' %i)
            #cv2.imwrite(path, original)
            path = os.path.join(graphImage, 'out_2_%s.png' %i)
            cv2.imwrite(path, seg)
            path = os.path.join(graphImage, 'out_3_%s.png' %i)
            cv2.imwrite(path, image)
        save_props(i, grafo1, csv_writer)
    
        failCount = 0
        for i in range(j+1, N):
            print(i)
            original = cv2.imread(raw_files[i])[p[0]:p[1],p[2]:p[3]]
            grafo2, seed, ske, ske2, seg = get_Graph(i-j, seg_files[i], p, seed, orig_seed)
            
            try:
                grafo1 = match_graph(grafo1, grafo2)
                if conf['SaveGraph']:
                    image = plot_graph(grafo1, original, ske)
                    #path = os.path.join(graphImage, 'out_1_%s.png' %i)
                    #cv2.imwrite(path, original)
                    path = os.path.join(graphImage, 'out_2_%s.png' %i)
                    cv2.imwrite(path, seg)
                    path = os.path.join(graphImage, 'out_3_%s.png' %i)
                    cv2.imwrite(path, image)
                save_props(i, grafo1, csv_writer)
                if failCount > 0:
                    failCount = 0
            except:
                failCount += 1
                if failCount > 5:
                    try:
                        grafo1 = graph_init(grafo1)
                    except:
                        pass
                print("ERROR")
                if conf['SaveGraph']:
                    image = plot_graph(grafo1, original, ske)
                    path = os.path.join(failImage, 'out_1_%s.png' %i)
                    cv2.imwrite(path, original)
                    path = os.path.join(failImage, 'out_2_%s.png' %i)
                    cv2.imwrite(path, seg)
                    path = os.path.join(failImage, 'out_3_%s.png' %i)
                    cv2.imwrite(path, image)
                save_props(i, grafo1, csv_writer)
    return

