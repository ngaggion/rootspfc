#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 29 14:55:32 2019

@author: ngaggion
"""
import tensorflow as tf
from modelUtils import conv2d, upsample2d, dropout, resUnit


class ResUNetDS(object):
  """
    Input has to be multiple of 4 (CHECK THIS).
  """
  def __init__(self, name, finetuneLayers = None, dropout = 0.5):
    self.name = name
    self.reuse = None
    self.finetuneLayers = finetuneLayers
    self.dropout = dropout

  def __call__(self, x, isTrain):
    "If finetuneLayers is None, all the layers will be finetuned. Otherwise, only those which match the names in finetuneLayers list will be trained."

    with tf.variable_scope(self.name, reuse=self.reuse):
      nonlinearity = tf.nn.elu

      # 128 x 128 (1) -> 128 x 128 (16)
      add1 = resUnit(x, 1, 16, 3, 1, "SAME", nonlinearity, isTrain)

      # 128 x 128 (16) -> 64 x 64 (32)
      add2 = resUnit(add1, 2, 32, 3, 2, "SAME", nonlinearity, isTrain)

      # 64 x 64 (32) -> 32 x 32 (64)
      add3 = resUnit(add2, 3, 64, 3, 2, "SAME", nonlinearity, isTrain)

      # 32 x 32 (64) -> 16 x 16 (128)
      add4 = resUnit(add3, 4, 128, 3, 2, "SAME", nonlinearity, isTrain)

      # 16 x 16 (128) -> 32 x 32 (64)
      drop4 = dropout(add4, self.dropout, isTrain, "drop4")
      up4 = conv2d(upsample2d(drop4, 2), "deconv4", 64, 3, 1, "SAME", bn=False, af=None, is_train=isTrain)
      concat5 = tf.add(up4, add3)
      add5 = resUnit(concat5, 5, 64, 3, 1, "SAME", nonlinearity, isTrain)
      
       # 32 x 32 (64) -> 64 x 64 (32)
      drop5 = dropout(add5, self.dropout, isTrain, "drop4")
      up5 = conv2d(upsample2d(drop5, 2), "deconv5", 32, 3, 1, "SAME", bn=False, af=None, is_train=isTrain)
      concat6 = tf.add(up5, add2)
      add6 = resUnit(concat6, 6, 32, 3, 1, "SAME", nonlinearity, isTrain)
      
      # 64 x 64 (32) -> 128 x 128 (16)    
      drop6 = dropout(add6, self.dropout, isTrain, "drop4")
      up6 = conv2d(upsample2d(drop6, 2), "deconv6", 16, 3, 1, "SAME", bn=False, af=None, is_train=isTrain)
      
      concat7 = tf.add(up6, add1)
      add7 = resUnit(concat7, 7, 16, 3, 1, "SAME", nonlinearity, isTrain)
      
#      unet_out1 = conv2d(add7, "unet_out", 2, 3, 1, "SAME", bn=True, af=None, is_train=isTrain)
#      unet_out = pixel_wise_softmax(unet_out1)

      unet_out = conv2d(add7, "unet_out", 2, 3, 1, "SAME", bn=False, af=tf.sigmoid, is_train=isTrain)

      stack = tf.concat([x, unet_out], axis = 3)

      conv8 = resUnit(stack, 8, 32, 5, 1, "SAME", nonlinearity, isTrain)
      conv9 = resUnit(conv8, 9, 32, 5, 1, "SAME", nonlinearity, isTrain)
      conv_out = conv2d(conv9, "conv_out", 2, 5, 1, "SAME", bn=True, af=None, is_train=isTrain)
      
    if self.reuse is None:

      if self.finetuneLayers is None:
          self.varList = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name)
      else:
          self.varList = []

          for scope in self.finetuneLayers:
            self.varList.extend(tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=scope))

      self.saver = tf.train.Saver(tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name))

      self.reuse = True

    return conv_out, unet_out

  def save(self, sess, ckpt_path):
    self.saver.save(sess, ckpt_path)

  def restore(self, sess, ckpt_path):
    self.saver.restore(sess, ckpt_path)


class ResUNet(object):
  """
    Input has to be multiple of 4 (CHECK THIS).
  """
  def __init__(self, name, finetuneLayers = None, dropout = 0.5):
    self.name = name
    self.reuse = None
    self.finetuneLayers = finetuneLayers
    self.dropout = dropout

  def __call__(self, x, isTrain):
    "If finetuneLayers is None, all the layers will be finetuned. Otherwise, only those which match the names in finetuneLayers list will be trained."

    with tf.variable_scope(self.name, reuse=self.reuse):
      nonlinearity = tf.nn.elu

      # 128 x 128 (1) -> 128 x 128 (16)
      add1 = resUnit(x, 1, 16, 3, 1, "SAME", nonlinearity, isTrain)

      # 128 x 128 (16) -> 64 x 64 (32)
      add2 = resUnit(add1, 2, 32, 3, 2, "SAME", nonlinearity, isTrain)

      # 64 x 64 (32) -> 32 x 32 (64)
      add3 = resUnit(add2, 3, 64, 3, 2, "SAME", nonlinearity, isTrain)

      # 32 x 32 (64) -> 16 x 16 (128)
      add4 = resUnit(add3, 4, 128, 3, 2, "SAME", nonlinearity, isTrain)

      # 16 x 16 (128) -> 32 x 32 (64)
      drop4 = dropout(add4, self.dropout, isTrain, "drop4")
      up4 = conv2d(upsample2d(drop4, 2), "deconv4", 64, 3, 1, "SAME", bn=False, af=None, is_train=isTrain)
      concat5 = tf.add(up4, add3)
      add5 = resUnit(concat5, 5, 64, 3, 1, "SAME", nonlinearity, isTrain)
      
       # 32 x 32 (64) -> 64 x 64 (32)
      drop5 = dropout(add5, self.dropout, isTrain, "drop4")
      up5 = conv2d(upsample2d(drop5, 2), "deconv5", 32, 3, 1, "SAME", bn=False, af=None, is_train=isTrain)
      concat6 = tf.add(up5, add2)
      add6 = resUnit(concat6, 6, 32, 3, 1, "SAME", nonlinearity, isTrain)
      
      # 64 x 64 (32) -> 128 x 128 (16)    
      drop6 = dropout(add6, self.dropout, isTrain, "drop4")
      up6 = conv2d(upsample2d(drop6, 2), "deconv6", 16, 3, 1, "SAME", bn=False, af=None, is_train=isTrain)
      
      concat7 = tf.add(up6, add1)
      add7 = resUnit(concat7, 7, 16, 3, 1, "SAME", nonlinearity, isTrain)
      
      conv_out = conv2d(add7, "conv_out", 2, 3, 1, "SAME", bn=True, af=None, is_train=isTrain)

    if self.reuse is None:

      if self.finetuneLayers is None:
          self.varList = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name)
      else:
          self.varList = []

          for scope in self.finetuneLayers:
            self.varList.extend(tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=scope))

      self.saver = tf.train.Saver(tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name))

      self.reuse = True

    return conv_out

  def save(self, sess, ckpt_path):
    self.saver.save(sess, ckpt_path)

  def restore(self, sess, ckpt_path):
    self.saver.restore(sess, ckpt_path)



class UNet(object):
  """
    This CNN takes an input image of a signal and preduces a dereverberated version of it.

    Input has to be multiple of 4 (CHECK THIS).
  """

  def __init__(self, name, finetuneLayers = None, dropout = 0.5):
    self.name = name
    self.reuse = None
    self.finetuneLayers = finetuneLayers
    self.dropout = dropout

  def __call__(self, x, isTrain):
    "If finetuneLayers is None, all the layers will be finetuned. Otherwise, only those which match the names in finetuneLayers list will be trained."

    with tf.variable_scope(self.name, reuse=self.reuse):
      nonlinearity = tf.nn.elu

      # 32 x 32 (1) -> 16 x 16 (16)
      conv1_a = conv2d(x, "conv1_a", 16, 3, 1, "SAME", bn=True, af=nonlinearity, is_train=isTrain)
      conv1_b= conv2d(conv1_a, "conv1_b", 16, 3, 1, "SAME", bn=True, af=None, is_train=isTrain)
      act1 = nonlinearity(conv1_b)
      pool1 = tf.nn.avg_pool(act1, [1, 2, 2, 1], [1, 2, 2, 1], "SAME")

      # 16 x 16 (16) -> 8 x 8 (32)
      conv2_a = conv2d(pool1, "conv2_a", 32, 3, 1, "SAME", bn=True, af=nonlinearity, is_train=isTrain)
      conv2_b = conv2d(conv2_a, "conv2_b", 32, 3, 1, "SAME", bn=True, af=None, is_train=isTrain)
      act2 = nonlinearity(conv2_b)
      pool2 = tf.nn.avg_pool(act2, [1, 2, 2, 1], [1, 2, 2, 1], "SAME")

      # 8 x 8 (32) -> 4 x 4 (64)
      conv3_a = conv2d(pool2, "conv3_a", 64, 3, 1, "SAME", bn=True, af=nonlinearity, is_train=isTrain)
      conv3_b = conv2d(conv3_a, "conv3_b", 64, 3, 1, "SAME", bn=True, af=None, is_train=isTrain)
      act3 = nonlinearity(conv3_b)
      pool3 = tf.nn.avg_pool(act3, [1, 2, 2, 1], [1, 2, 2, 1], "SAME")

      # 4 x 4 (64) -> 2 x 2 (128)
      conv4_a = conv2d(pool3, "conv4_a", 128, 3, 1, "SAME", bn=True, af=nonlinearity, is_train=isTrain)
      conv4_b = conv2d(conv4_a, "conv4_b", 128, 3, 1, "SAME", bn=True, af=None, is_train=isTrain)
      act4 = nonlinearity(conv4_b)

      # Dropout
      drop4 = tf.layers.dropout(act4, rate=self.dropout, training=isTrain, name="drop4")

      # Up-scaling and convolving without non-linearity
      # 2 x 2 (128) -> 4 x 4 (64)
      deconv4= conv2d(upsample2d(drop4, 2), "deconv4", 64, 3, 1, "SAME", bn=True, af=None, is_train=isTrain)

      # 4 x 4 (64) -> 8 x 8 (32)
      concat5 = nonlinearity(tf.add(deconv4, conv3_b))
      conv5_a = conv2d(concat5, "conv5_a", 64, 3, 1, "SAME", bn=True, af=nonlinearity, is_train=isTrain)
      conv5_b = conv2d(conv5_a, "conv5_b", 64, 3, 1, "SAME", bn=True, af=nonlinearity, is_train=isTrain)
      drop5 = tf.layers.dropout(conv5_b, rate=self.dropout, training=isTrain, name="drop5")
      deconv5= conv2d(upsample2d(drop5, 2), "deconv5", 32, 3, 1, "SAME", bn=True, af=None, is_train=isTrain)

      # 8 x 8 (32) -> 16 x 16 (16)
      concat6 = nonlinearity(tf.add(deconv5, conv2_b))
      conv6_a = conv2d(concat6, "conv6_a", 32, 3, 1, "SAME", bn=True, af=nonlinearity, is_train=isTrain)
      conv6_b = conv2d(conv6_a, "conv6_b", 32, 3, 1, "SAME", bn=True, af=nonlinearity, is_train=isTrain)
      drop6 = tf.layers.dropout(conv6_b, rate=self.dropout, training=isTrain, name="drop6")
      deconv6= conv2d(upsample2d(drop6, 2), "deconv6", 16, 3, 1, "SAME", bn=True, af=None, is_train=isTrain)

      # 16 x 16 (16) -> 32 x 32 (2)
      concat7 = nonlinearity(tf.add(deconv6, conv1_b))
      conv7_a = conv2d(concat7, "conv7_a", 16, 3, 1, "SAME", bn=True, af=nonlinearity, is_train=isTrain)
      conv7_b = conv2d(conv7_a, "conv7_b", 16, 3, 1, "SAME", bn=True, af=nonlinearity, is_train=isTrain)
      conv_out = conv2d(conv7_b, "conv_out", 2, 3, 1, "SAME", bn=True, af=None, is_train=isTrain)

    if self.reuse is None:

      if self.finetuneLayers is None:
          self.varList = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name)
      else:
          self.varList = []

          for scope in self.finetuneLayers:
            self.varList.extend(tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=scope))

      self.saver = tf.train.Saver(tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name))

      self.reuse = True

    return conv_out

  def save(self, sess, ckpt_path):
    self.saver.save(sess, ckpt_path)

  def restore(self, sess, ckpt_path):
    self.saver.restore(sess, ckpt_path)

