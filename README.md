# Desarrollo de una herramienta para segmentación y caracterización de raíces de *Arabidopsis thaliana* en series temporales de imágenes.

Proyecto Final de Carrera de Ingeniería en Informática.
Alumno: Gaggion Zulpo, Rafael Nicolás

Para su instalación por Anaconda ingresar por línea de comandos:

`conda env create -f environment.yml`

En caso de no utilizar anaconda, instalar las siguientes dependencias:
  - python=3.7.6
  - graph-tool=2.29
  - matplotlib=3.1.2
  - opencv=4.2.0
  - pillow=7.0.0
  - scikit-image=0.16.2
  - scipy=1.4.1
  - medpy=0.4.0
  - numpy=1.18.1
  - tensorflow=1.15.0

Para su utilización, ingresar los paths necesarios en Codigo/config_file.conf y luego ingresar por línea de comando:

`python plantRootAnalyzer.py`

Demo:
[https://youtu.be/FmiC0Kg12xQ](https://youtu.be/FmiC0Kg12xQ)